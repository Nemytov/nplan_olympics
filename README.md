1. First, set the correct absolute path in story/story\_template.tex. 
2. Next, in setup\_tex.py check LaTeX parameters match your system. 
3. Then run dt\_main.py. Tested using Python 2.7
4. Finally, view story/story.pdf (in Landscape + presentation (fullscreen) mode)
